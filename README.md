# Indent Type Detection

Indent Type Detection is intended to automatically determine indentation
size in a document with Java (in fact not only with Java) source code.

## Description

This project implements the test task given during 
[JetBrain's Internship 2018](https://jetbrains.ru/students/internship/) for 
[.editorconfig language support](https://jetbrains.ru/students/internship/themes/editorconfig-language-support/)
project.

The initial task was:
> Write a program for automatic detection of indentation size for a document 
  with Java source code.
  Consider that file can be indented not only with whitespaces, nor it's
  guaranteed to be properly formatted.
  Program should take file path as an argument and print two values:
  symbol name used for indentation, and indentation size.
>
> Write an explanatory comment in russian or english with ideas you followed
  and cases you wanted to support.
>
> Program can be written in Java, Kotlin, or C#.
> Attach .zip archive with your project, add a link to a private repository
  (alexander.kirsanov@jetbrains.com), or paste the source to the assignment
  form marked as code block.

## Getting Started

To run the program just compile it with
[IntelliJ IDEA](https://kotlinlang.org/docs/tutorials/getting-started.html)
or [kotlinc](https://kotlinlang.org/docs/tutorials/command-line.html) and
run providing the file name to parse in the first program argument.

## How indents are determined

To determine indent size and type (spaces and/or tabs) the program parses
given file line by line and detects amount of leading spaces and/or tabs
on each line, except of comment and blank lines.

Let `a` and `b` be strings with leading spaces/tabs for the last processed
string and the current sting respectively.

If `b` starts with `a` and greater than `a`, then it have an indent. Indent
size is `b.length - a.length` and indent type is corresponds to symbols
of `b.removePrefix(a)`.

If `a` starts with `b` and greater than `b`, then `b` have a dedent. Dedent
size is `a.length - b.length` and dedent type is corresponds to symbols
of `a.removePrefix(b)`.

The considered indent size and type is the most used indent or dedent size
and type in the whole file.

For the examples see the [examples](./examples) folder.
