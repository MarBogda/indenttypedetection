package indentTypeDetection

/**
 * Used to detect indent of the processed string.
 *
 * Instance of the class acts like function with state (overloading [invoke]
 * operator), so the results of different strings are depend on each other
 * (this is because function detects additional indent, i.e. indent started
 * just after the end of the indent of the previous string, not started at
 * start of the line).
 *
 * @property prevStringIndent Full indent of the previous string.
 */
internal class GetIndent {
    private var prevStringIndent = ""

    /**
     * Returns the (additional) indent of the one-line [string], or dedent
     * of this string, or `null`.
     *
     * [String][string] is considered containing indent if it's full indent
     * starts with [prevStringIndent] and larger than it, or if it's full
     * indent does not start with [prevStringIndent] and is not empty.
     *
     * [String][string] is considered containing dedent if
     * [prevStringIndent] starts with [string]'s full indent.
     *
     * [String][string] is considered not containing indent or dedent
     * if it's full indent is the same as [prevStringIndent].
     *
     * Format of dedent is the same as indent, e.g. dedent of 4 spaces
     * returns object [Indent](4, [IndentType.SPACE]).
     */
    operator fun invoke(string: String): Indent? {
        /**
         * The full indentation (i.e. previous indentation (if [string]
         * contains it) concatenated with additional indentation).
         */
        val currentStringIndent = string.takeWhile { it == ' ' || it == '\t' }
        val result = if (prevStringIndent.startsWith(currentStringIndent)) {
            // There is either dedent (i.e. the end of indented block) or
            // no additional indentation (in case `prevStringIndent ==
            // currentStringIndent`, for which result is null).
            Indent.fromIndentString(
                prevStringIndent.drop(currentStringIndent.length)
            )
        } else {
            // There is the additional indentation.
            Indent.fromIndentString(
                currentStringIndent.removePrefix(prevStringIndent)
            )
        }

        prevStringIndent = currentStringIndent
        return result
    }
}
