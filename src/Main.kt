package indentTypeDetection

import java.io.File
import java.io.FileNotFoundException

/**
 * The entry point of program.
 *
 * Reads the provided file line by line, detects and prints it's indent type
 * (the character name and the indentation size).
 *
 * @param args Program arguments. Used only the first one (i.e. `args[0]`)
 * to specify the file to parse.
 */
fun main(args: Array<String>) {
    println(
        "Welcome to the program for automatic detection of indentation size " +
                "for a document with Java source code."
    )

    // Ensures that file name is passed and gets it into `filepath` variable.
    if (args.isEmpty()) {
        println("No argument is provided. Please provide file name to parse.")
        return
    } else if (args.size > 1) {
        println("More than one argument is provided, using only the first one.")
    }
    val filepath = args[0]

    /* Reads a file line by line, checks the line is not blank or
     * comment, gets line's indent and store it in a map counter. So, after
     * this step all file's indents and their amounts will be found.
     */

    /** Map to store detected indents and their amounts. */
    val indents = HashMap<Indent, Int>()
    val file = File(filepath)
    val blankOrComment = BlankOrComment()
    val getIndent = GetIndent()
    try {
        file.forEachLine { line: String ->
            // Process only code lines (i.e. not blank or comment lines).
            if (blankOrComment(line))
                return@forEachLine
            // Incrementing amount of current indents if indent is not `null`.
            getIndent(line)?.let {
                indents[it] = indents.getOrDefault(it, 0) + 1
            }
        }
    } catch (e: FileNotFoundException) {
        println("Specified file not found.")
        return
    }
    // If there is unclosed indent then close it (get dedent) else do nothing.
    getIndent("")?.let { indents[it] = indents.getOrDefault(it, 0) + 1 }

    // Handling empty, blank or only comments file.
    if (indents.isEmpty()) {
        println("File is empty or consists only of blank or comment lines.")
        return
    }

    /* After detection of different indents and their amounts, detects
     * the most used indent and prints it. If the most used indent has `MIXED`
     * type, then finds the next one (and so on), but prints the message about
     * existence of many mixed indents. If there are several the most used
     * indents, prints all of them (except ones with `MIXED` type).
     */

    /** Stores array of mostly used indents. */
    val maxIndents = arrayListOf<Indent>()
    /**
     * Stores amount of mostly used indents, which type
     * ([IndentType]) is [IndentType.SPACE] or [IndentType.TAB]).
     */
    var maxAmount = 0
    /** Stores amount of mostly used [IndentType.MIXED] indents. */
    var maxMixedAmount = 0
    for ((indent, amount) in indents) {
        if (indent.type == IndentType.MIXED) {
            // Maximum amount of indents with `MIXED` type is collected
            // separately from others without information about indent's length.
            maxMixedAmount = Integer.max(maxMixedAmount, amount)
        } else if (amount >= maxAmount) {
            // Maximum amount of `SPACE` or `TAB` indents is collected
            // with information about indent's length and type.
            if (amount > maxAmount) {
                maxIndents.clear()
                maxAmount = amount
            }
            maxIndents.add(indent)
        }
    }
    if (maxIndents.isNotEmpty()) {
        // There're found some `SPACE` or `TAB` indents.

        if (maxIndents.size == 1) { // Only one leader indent.
            println("The most used indent is ${maxIndents[0]}.")
        } else { // Several leaders.
            println(
                "The most used indents are: ${maxIndents.joinToString()}."
            )
        }
    } else { // No `SPACE` or `TAB` indents found, exist only `MIXED` ones.
        println("Can't detect indent of the file.")
    }
    // If there is `MIXED` indent with amount greater than or equal to
    // maximum amount of `SPACE` or `TAB` indents then detected indent
    // may not be proper.
    // This condition is always `true` if exist only `MIXED` indents.
    if (maxMixedAmount >= maxAmount) {
        println(
            "But there are a lot of mixed " +
                    "(i.e. a mess of spaces and tabs) indents."
        )
    }
}
