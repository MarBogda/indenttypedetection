package indentTypeDetection

/**
 * Enum class for indent types.
 *
 * Using indent types are:
 * [SPACE] if indent consists only of space characters,
 * [TAB] if indent consists only of tab characters,
 * [MIXED] if indent consists both of space and tab characters.
 *
 * @property char Contains char correspond to current indent type.
 */
internal enum class IndentType(val char: Char?) {
    SPACE(' '), TAB('\t'), MIXED(null);

    override fun toString(): String {
        return name.toLowerCase()
    }

    /**
     * Used to get IndentType from a character (' ', '\t' or null).
     *
     * @property map Contains mapping from Char to IndentType.
     * */
    companion object {
        private val map = IndentType.values().associateBy(IndentType::char)

        /**
         * Returns IndentType correspond to given [char].
         *
         * [Char][char] should be either space (`' '`), tab(`'\t'`) or `null`.
         * Otherwise function returns `null`.
         */
        fun fromChar(char: Char?) = map[char]
    }
}

/**
 * Data class for indent.
 *
 * Contains indent [type] and [length], so indents with
 * the same type and length are considered equal.
 */
internal data class Indent(
    val length: Int,
    val type: IndentType = IndentType.SPACE
) {
    override fun toString(): String {
        return "$length $type character${if (length > 1) "s" else ""}"
    }

    companion object {
        /**
         * Returns [Indent] object corresponds to given [string].
         *
         * [String][string] should not be empty and should consist of spaces
         * (`' '`) and/or tabs (`'\t'`). Otherwise function returns `null`.
         */
        fun fromIndentString(string: String): Indent? {
            if (string.isEmpty()) {
                return null
            }
            var type = IndentType.fromChar(string.first()) ?: return null
            // The side effect of statement below is that `type` is `MIXED`
            // if there are both spaces and tabs in the string.
            val allSpacesOrTabs = string.all {
                if (it != type.char) {
                    type = IndentType.MIXED
                }
                it == ' ' || it == '\t'
            }
            return if (allSpacesOrTabs) {
                Indent(string.length, type)
            } else {
                null
            }
        }
    }
}
