package indentTypeDetection

/**
 * Used to consider if the processed string is blank or comment or not.
 *
 * Instance of the class acts like function with state (overloading [invoke]
 * operator), so the results of different strings are depend on each other
 * (this is because comments in file can be multiline whereas program processes
 * file line by line for memory purposes).
 *
 * @property openBlockComment Shows if there was an open block comment before.
 * @property inlineComment Regex for matching an inline block comment or
 * line comment.
 * @property startBlockComment Regex for matching the start of a block comment.
 * @property endBlockComment Regex for matching the end of a block comment.
 * @property endAndStartBlockComment Regex for matching the end of a block
 * comment and then the start of an another block comment.
 */
internal class BlankOrComment {
    private var openBlockComment = false

    /**
     * Returns if the one-line [string] is blank or comment or not.
     *
     * [String][string] is considered comment if there was an open block
     * comment before, or [string] starts with a comment (even if after the
     * comment there are non-comment code - because it's pointless to
     * determine indent of the string starts with the comment).
     */
    operator fun invoke(string: String): Boolean {
        /** String without any inline block comments or line comments. */
        val withoutInlineComments = inlineComment.replace(string, "")

        return if (openBlockComment) { // There was an open block comment.
            // If there is the end of the block comment, then
            // `openBlockComment` for the next line is false.
            // But if there is also the start of an another block comment,
            // then `openBlockComment` for the next line is true.
            //
            // There is not any collisions (e.g., `...*/.../*...*/`
            // is matched as an open block comment whereas it is not),
            // because inline block comments and line comments are removed.
            openBlockComment =
                    !endBlockComment.containsMatchIn(withoutInlineComments)
                    || endAndStartBlockComment.containsMatchIn(
                withoutInlineComments
            )

            // Since there was the open block comment, then return `true`
            // because at least the line's prefix (or maybe the hole line)
            // is a part of the block comment.
            true
        } else { // There wasn't an open block comment.
            // If there is the start of an block comment, then
            // `openBlockComment` for the next line is true.
            // Multiple starts of block comment handled as one block
            // (block comments are not nested in Java).
            // There can't be the end of the block comment after the start
            // or the start of the block comment after line comment,
            // because inline block comments and line comments are removed.
            openBlockComment =
                    startBlockComment.containsMatchIn(withoutInlineComments)

            // If the string is blank or it starts with line or block comment,
            // then return `true`. Return `false` otherwise.
            val trimmedString = string.trimStart()
            trimmedString.isBlank()
                    || trimmedString.startsWith("//")
                    || trimmedString.startsWith("/*")
        }
    }

    companion object {
        private val inlineComment = Regex("/\\*.*?\\*/|//.*\$")
        private val startBlockComment = Regex("/\\*")
        private val endBlockComment = Regex("\\*/")
        private val endAndStartBlockComment = Regex("\\*/.*?/\\*")
    }
}
