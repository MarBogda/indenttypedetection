// There is no indent.
public class Main {
// 4 spaces indent.
    public static void main(String[] args) {
// 1 tab indent (take into account only added indent).
    	int a = 0;
// There is no indent (i.e. using previous indent).
    	for (char c : args[0].toCharArray()) {
// 3 mixed indent (2 spaces and 1 tab).
    	  	if (a > 0) {
// 2 mixed indent (1 space and 1 tab).
    	  	 	a += 1;
// 2 mixed dedent (1 space and 1 tab).
// Considered dedent if the full indent is a prefix of previous indent.
    	  	} else
// New indentation: neither the previos indent starts with the full new one,
// nor the full new indent starts with the previos one.
// So there is 8 mixed indent (4 spaces + 1 tab + 1 space + 1 tab + 1 space).
    	 	 if (a < 0)
// 2 mixed indent (1 tab and 1 space).
    	 	 	 a -= 1;
// New indentation: 8 mixed indent (4 spaces + 1 tab + 2 spaces + 1 tab).
    	  	else if (c == ' ') {
// 2 mixed indent (1 space and 1 tab)
    	  	 	System.out.println("kuku");
// 2 mixed dedent (1 space + 1 tab).
    	  	}
// 5 mixed dedent (1 space + 1 tab + 2 spaces + 1 tab). Note: not check
// if indent size after dedent is the same as in the start of the block.
   }
// 2 mixed indent (1 space + 1 tab).
    	System.out.println("hello");
// 1 tab dedent.
    }
// 4 spaces dedent.
}
// indents/dedents summary:
// two '1 tab', two '4 spaces', one '3 mixed',
// six '2 mixed', two '8 mixed', one '5 mixed'.
