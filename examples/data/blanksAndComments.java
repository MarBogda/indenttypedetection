// Comment lines are not processed.
public class Main {
  	// Even if there are some spaces/tabs before them.
    public static void main(String[] args) {
        int a = 0;
        // Blank lines are not processed too
        // (note that lines below consist of spaces/tabs)
  			     		     
                			             	
	       
        // /* Note that it's not the start of the block comment (this is in the line comment).
        for (char c : args[0].toCharArray()) {
                /* Block comments
                 also
                  are not processed
                  /* (remember that java hasn't nested block comments). /* /* /*
                    */ // /* It's not the start of the block comment again.
            if (a > 0) {
                a /* But inline block comments are processed...*/+= 1;
		        /*...if they are not at the start of the line.*/} else
            if (a < 0)
      	        /*Code lines start with block comment*/a -= 1;/*are not processed.*/
            else /*You can comment something here*/if (c ==/*and here*/ ' ') {/*and here
                        and all the rest
                            will be commented
                                  until end of commet block
                (the line below is not processed too - it starts before the block comment is ended).
                */System.out/*and now you can comment again*/.println/*and again*/("kuku");/* and again
                  again again again
                (the line below is processed - block comment is ended before the line statrs).*/
                System.out.println("hello");
            }
        }

    } // There is no indent (line above is not processed).
}
// So there are only five '4 spaces' indents (lines 4, 5, 18, 19, 31)
// and five '4 spaces' dedents (lines 21, 32, 33, 35, 36).
