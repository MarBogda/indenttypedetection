# Examples

This folder contains examples (*.java files in the [./data](./data) folder)
of how the program processed various corner cases during detecting indent size.

Corresponding files with the *.output extension in the [./output](output)
folder contains the program output for the example files.

## [empty](data/empty.java)

This file contains nothing, so the program could not detect the indent size.

## [blank](data/blank.java)

This file contains only blank lines (consists of only spaces or tabs), so
the program could not detect the indent size.

## [comments](data/comments.java)

This file contains only comment lines, so the program could not detect
the indent size.

## [fourSpaces](data/fourSpaces.java)

This file contains simple (and senseless) Java program with four spaces used
to indent.

## [oneTab](data/oneTab.java)

This file contains the same senseless Java program with one tab used to indent.

## [blanksAndComments](data/blanksAndComments.java)

This file is intended to explain how the program handles blank and comment
lines in the parsed file.

## [spacesAndTabs](data/spacesAndTabs.java)

This file is intended to explain how the program counts various indents/dedents
consist of spaces and/or tabs and detects the most used one.

## [dedents](data/dedents.java)

This file contains the case when there are more dedents than indents
in the file.

## [pieceOfCode](data/pieceOfCode.java)

This file contains some random piece of code from the Internet (in fact it is
C++ code, not Java) and explains, how the program detects it's indent size.
